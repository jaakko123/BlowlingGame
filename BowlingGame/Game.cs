﻿using System;
using System.Collections.Generic;

namespace BowlingGameTests
{
    public class Game
    {
        private List<int> rolls = new List<int>(21);
        public void Roll(int pinsHit)
        {
            rolls.Add(pinsHit);
        }
        public int Score
        {
            get
            {
                int score = 0;
                int framei = 0;
                for (int frame = 0; frame < 10; frame++)
                {
                    if (IsStrike(framei))
                    {
                        score += 10 + rolls[framei + 1] + rolls[framei + 2];
                        framei++;
                    }
                    else if (IsSpare(framei))
                    {
                        score += 10 + rolls[framei + 2];
                        framei += 2;
                    }
                    else
                    {
                        score += rolls[framei] + rolls[framei + 1];
                        framei += 2;
                    }
                }
                return score;
            }
        }

        private bool IsStrike(int framei)
        {
            return rolls[framei] == 10;
        }

        private bool IsSpare(int framei)
        {
          return rolls[framei] + rolls[framei + 1] == 10;
        }

    }


}