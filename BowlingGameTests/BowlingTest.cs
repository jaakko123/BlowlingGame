﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// mallia kopioitu https://www.slideshare.net/stewshack/bowling-game-kata-c sivun kautta
namespace BowlingGameTests
{
    [TestFixture]
    public class BowlingTest
    {
        [Test]
        public void GutterBallTest()
        {
            var game = SetupGame();
            RollPins(game, 20, 0);
            Assert.AreEqual(0, game.Score);
        }
 
    [Test]
        public void OnePinHitTest()
        {
            var game = SetupGame();
            RollPins(game, 20, 1);
            Assert.AreEqual(20, game.Score);
        }

    [Test]
        public void OneSpareTest()
        {
            var game = SetupGame();
            rollSpare(game);
            game.Roll(3);
            RollPins(game, 17, 0);
            Assert.AreEqual(16, game.Score);
        }
        [Test]
        public void StrikeTest()
        {
            var game = SetupGame();
            game.Roll(10);
            game.Roll(3);
            game.Roll(6);
            RollPins(game, 16, 0);
            Assert.AreEqual(28, game.Score);
        }
        [Test]
        public void PerfectTest()
        {
            var game = SetupGame();
            RollPins(game, 12, 10);
            Assert.AreEqual(300, game.Score);
        }

        private void rollSpare(Game game)
        {
            game.Roll(5);
            game.Roll(5);
        }

        private Game SetupGame()
        {
            return new Game();
        }
        private void RollPins(Game game, int numberofRolls, int PinsHitPerRoll)
        {
            for (int i = 0; i < 20; i++)
            {
                game.Roll(PinsHitPerRoll);
            }
        }
    }
}
